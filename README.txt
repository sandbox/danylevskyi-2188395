Commerce Stock By Role
======================
Provides "Update commerce stock field" permission to the commerce_stock field.
It can be useful if you would like to grant only specific role with ability
to update commerce stock field.

Installation and configuration
==============================
1. Download commerce_stock_by_role.
2. Enable the Commerce Stock API module.
3. Go to "people > permissions" and set desirable roles in front
   of "Update commerce stock field" permission.
